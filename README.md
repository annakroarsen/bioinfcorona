## Local setup

### Installation with examples for ubuntu. Windows and OSX is mostly the same

Fork the project and clone it to your machine.

#### Setup and activation of virtualenv (env that prevents python packages from being installed globaly on the machine)
Naviagate into the project folder.

`pip install virtualenv`

`virtualenv env`

**Activate environment:**

For mac/linux:

`source env/bin/activate`

For windows:

`env\Scripts\activate.bat`

If you get an error related to charmap on Windows, run this command:
`set PYTHONIOENCODING=UTF-8`


#### Install python requirements

`pip install -r requirements.txt`

**Note: Mac users may get an error related to psycopg2. To fix this try running:**

`pip install psycopg2==2.7.5`

- if you got the error you may need to run the install requirments again to get it working properly

#### Migrate database

- access the folder where the manage.py files is

`python manage.py migrate`


#### Create superuser

Create a local admin user by entering the following command:

`python manage.py createsuperuser`

Only username and password is required


#### Start the app

`python manage.py runserver`

- Remember to always activate the environment before running the app, if you have closed the terminal.
- Some may need to write pip3 and python3 to get correct versions and to make it function correctly


