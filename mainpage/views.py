from django.shortcuts import render, redirect
from django.contrib import messages 
from .models import InputSequence, DisplayTable
from .forms import InputSequenceForm

from datetime import date

# Create your views here.

def tableview(request): 
    library = DisplayTable.objects.all()
    return render(request, 'mainpage/tablepage.html',
    {
       'library':library, 
    })


def isFasta(sequence):
    if(sequence[0]=='>'):
        return True
    else:
        return False


def inputsequence(request):
    sequences = InputSequence.objects.values_list('sequence')
    print(sequences)
    if request.method == 'POST':
        form = InputSequenceForm(request.POST)
        input_sequence = request.POST['sequence']
        
        if not isFasta(input_sequence):
            messages.add_message(request, messages.INFO, 'You need to add a FASTA sequence beginning with >')
            form = InputSequenceForm()
            return render(request, 'mainpage/mainpage.html', {'form': form})


        sequence_info_input = prepareSequence(input_sequence)
        input_cleaned = sequence_info_input[3]

        #sjekker om string eller substring finnes
        sequence_success=False
        sequence_index=0 #hvilken sekvens vi ser på nå
        existing_sequence=""

        substring=""
        longest_subsequence= [''] * 3

        while (sequence_success==False and sequence_index<len(sequences)):
            
            existing_sequence = sequences[sequence_index][0]
            sequence_info_existing = prepareSequence(existing_sequence)
            existing_cleaned = sequence_info_existing[3]

            print(sequence_info_existing[0])
            
            #sjekker om eksakt samme string finnes
            if input_cleaned == existing_cleaned:
                print("sequences are the same")

                #checks if they have the same identifier
                if (sequence_info_input[0] == sequence_info_existing[0]):
                    messages.add_message(request, messages.INFO, 'This sequence is already in the library')
                else:
                    messages.add_message(request, messages.INFO, 'An identical sequence with a different identifier is already in the library')
                    updateAlignmentFields(sequence_info_existing, sequence_info_input)
                
                sequence_success=True
                break
                

            #sjekker om den er en subsekvens av eksisterende kar 
            elif input_cleaned in existing_cleaned:
                messages.add_message(request, messages.INFO, 'This sequence is a substring of an existing string')
                updateAlignmentFields(sequence_info_existing, sequence_info_input)
                sequence_success=True
                break
            

            #sjekker om den nye sekvensen er lengre og inneholder en eksisterende sekvens i tabellen
            elif existing_cleaned in input_cleaned:
                messages.add_message(request, messages.INFO, 'An existing sequence is a substring of this sequence, this sequence will replace the existing sequence')
                InputSequence.objects.filter(sequence=existing_sequence).update(sequence=input_sequence)
                updateAllFields(sequence_info_existing, sequence_info_input, input_cleaned)
                sequence_success=True
                break


            #må også sjekke om de er subsekvenser av hverandre, altså at deler er like, da skal de også kombineres
            threshold = len(existing_cleaned)*0.05
            substring = longestCommonSubstring(input_cleaned, existing_cleaned, threshold)
            
            if(substring[0]!=""):
                longest_subsequence = compareWithCurrentLongest(substring, longest_subsequence, input_cleaned, sequence_info_existing)

            
            #sjekker om vi har gått gjennom alle sekvensene og om det finnes noen subsekvens
            if(sequence_index == (len(sequences)-1) and len(longest_subsequence[2]) > 0):
                original_sequence_info, new_sequence = substringconcat(longest_subsequence)
                updateAllFields(original_sequence_info, sequence_info_input, new_sequence)
                messages.add_message(request, messages.INFO, 'Common substring with existing substring, alignment conducted')
                sequence_success=True

            sequence_index +=1
            print(sequence_index)
       
        if form.is_valid() and not sequence_success:
            print("fant ikke string fra før og form var valid")
            messages.add_message(request, messages.SUCCESS, 'Your sequence was added successfully to the library')
            sequence = form.save(commit=False)
            sequence.save()
            createNewField(sequence_info_input)

        form = InputSequenceForm()
        return redirect('tableview')
    else:
        form = InputSequenceForm()
    return render(request, 'mainpage/mainpage.html', {'form': form})



def prepareSequence(sequence):
    element_list = sequence.split('|')
    print(element_list, len(element_list))

    identifier = element_list[0]
    species = element_list[1]
    lastpart = element_list[2]

    nametype=""
    i=0
    while(lastpart[i].islower() or lastpart[i]==' '):
        print(lastpart[i])
        nametype += lastpart[i]
        i += 1
    
    cleaned = lastpart[i:]
    print("nametype:", nametype)

    return [identifier, species, nametype, cleaned]
    
def getAlignmentNumber(sequence):
    current_alignment_number = 0
    for info in sequence:
        current_alignment_number = int(info.alignment_number)
    print(current_alignment_number)

    return current_alignment_number 


def updateAlignmentFields(list_existing, list_input):
    sequence = DisplayTable.objects.filter(uniqueIdentifier=list_existing[0])
    print(sequence)
    current_alignment_number = getAlignmentNumber(sequence)

    sequence.update(alignment_number=current_alignment_number+1, latest_aligned_with = list_input[0], latest_alignment_date = date.today())


def updateAllFields(list_existing, list_input, new_sequence):
    sequence = DisplayTable.objects.filter(uniqueIdentifier=list_existing[0])
    print("identifer:", list_existing[0])
    current_alignment_number = getAlignmentNumber(sequence)
    sequence.update(uniqueIdentifier=list_existing[0], species=list_input[1], nametype=list_input[2], sequence_cleaned = sequence, alignment_number=current_alignment_number+1, latest_aligned_with = list_existing[0], latest_alignment_date = date.today())

def createNewField(list_input):
    DisplayTable.objects.create(uniqueIdentifier = list_input[0], species = list_input[1], nametype=list_input[2], sequence_cleaned= list_input[3], dateadded = date.today(), latest_alignment_date=date.today())



def longestCommonSubstring(sequence1, sequence2, threshold):
    length1 = len(sequence1)
    length2 = len(sequence2)

    lcs_table  = [[0 for k in range(length2+1)]for l in range(length1+1)]
    

    #length of longest common substring
    length=0
    row, column = 0, 0
    

    for i in range(length1 + 1):
       
        for j in range(length2 + 1):
            if(i==0 or j==0):
                lcs_table[i][j] = 0
                
            elif(sequence1[i-1] == sequence2[j-1]):
                lcs_table[i][j] = lcs_table[i-1][j-1] + 1
                
                if (length < lcs_table[i][j]):
                    length = lcs_table[i][j]
                    row = i
                    column = j 
            else:
                lcs_table[i][j] = 0

    #sjekker om det er noen substring som er har en lengde større en angitt threshold
    if length<threshold or length==0:
        print("no common substring")
        return [""]

    common_sequence = ['0'] * length 
    
    #finner indeksen der subsekvensen starter
    sequence1_index = row-length
    sequence2_index = column-length

    print(sequence1_index, sequence2_index)

    while lcs_table[row][column] != 0:
        length -=1
        common_sequence[length] = sequence1[row - 1]
        print("result:", common_sequence)

        row-= 1
        column -= 1

    print("result:", common_sequence)
    
    return [(''.join(common_sequence)), sequence1_index, sequence2_index]


def compareWithCurrentLongest(substring, longest_subsequence, input_sequence, existing_sequence_info):
    if(len(substring[0]) > len(longest_subsequence[2]) and len(substring[0])>0):
            longest_subsequence[0]  = input_sequence
            longest_subsequence[1]  = existing_sequence_info
            longest_subsequence[2]  = substring
    
    return longest_subsequence



def substringconcat(longest_subsequence):
    index1 = int(longest_subsequence[2][1])
    index2 = int(longest_subsequence[2][2])

    print("index1:", index1, "index2:", index2)

    sequence1 = longest_subsequence[0]
    sequence2 = str(longest_subsequence[1][3])

    substring_length = len(longest_subsequence[2][0])
    new_sequence=""

    print("dette:", index1+substring_length)
    print("videre, ", sequence1[:(index1+substring_length)])
    
    if(index1>index2):
        new_sequence = sequence1[:(index1+substring_length)] + sequence2[(index2+substring_length):]
    else:
        new_sequence = sequence2[:(index2+substring_length)] + sequence1[(index1+substring_length):]

    original_sequence_info = longest_subsequence[1]
    
    return original_sequence_info, new_sequence