from django.db import models

# Create your models here.


class InputSequence(models.Model):
    sequence=models.CharField(max_length=100000)

    def __str__(self):
        return self.sequence



class DisplayTable(models.Model):
    uniqueIdentifier=models.CharField(max_length=200)
    species=models.CharField(max_length=200)
    nametype = models.CharField(max_length=200)
    sequence_cleaned = models.CharField(max_length=100000)
    dateadded= models.DateField(default = None)
    alignment_number = models.IntegerField(default=0)
    latest_aligned_with = models.CharField(max_length=100000, default="")
    latest_alignment_date = models.DateField(default=None)



    def __str__(self):
        return self.uniqueIdentifier + " " + str(self.alignment_number)
