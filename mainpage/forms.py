from django import forms
from .models import InputSequence

class InputSequenceForm(forms.ModelForm):
    sequence= forms.CharField(max_length=100000)

    class Meta:
        model = InputSequence
        fields = ('sequence',)