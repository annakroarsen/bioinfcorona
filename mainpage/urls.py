from django.urls import path
from . import views

urlpatterns = [
    path('', views.inputsequence, name='inputsequence'),
    path('table', views.tableview, name='tableview'),

]