from django.contrib import admin
from .models import InputSequence, DisplayTable
# Register your models here.


admin.site.register(InputSequence)
admin.site.register(DisplayTable)